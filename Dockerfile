FROM python:3.9.10-slim-buster

WORKDIR /vendomatic

COPY requirements.txt requirements.txt
RUN apt update && apt install gcc -y
RUN pip3 install -r requirements.txt

COPY . .

CMD ["python3", "-m", "flask", "--app", "server", "run", "--host=0.0.0.0"]