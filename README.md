# Vend O Matic
I created a very simple model for this sample flask application. 

We just have a simple REST API in Flask which does all of the input validation. 
Then the vendomatic class that handles keeping track of credits and inventory.

EX:
```mermaid
sequenceDiagram
    Client->>REST Ingress: Insert coin
    REST Ingress->>VendOMatic Instance: Add 1 credit
    Client->>REST Ingress: Insert coin
    REST Ingress->>VendOMatic Instance: Add 1 credit
    Client->>REST Ingress: Purchase beverage 2
    REST Ingress->>VendOMatic Instance: Remove 2 credits, remove product from inventory
```

# Building
## Manually
```bash
python -m pip install -r requirements.txt
python -m flask --app server run --host=0.0.0.0
```

## Docker
```bash
docker build -t vendomatic .
docker run --rm -p 8080:5000 --name vendomatic vendomatic
```

# Integration Tests
I have a simple integration tests file that's wired up to talk to the docker container running on port 8080.
You can run it by doing the following:
```bash
python integration_tests.py
```

it should be run on a freshly started server and it will do a bunch of test cases. It's basic but works for such a small project.