import requests
import re
# Run after restarting the flask development server
# These are super hacky tests, we are just making calls and checking status codes

URL = "http://127.0.0.1:8080/"


def add_coin():
    payload = {"coin": 1}
    return requests.put(URL, data=payload)


def reset_purchase():
    return requests.delete(URL)


def get_inventory():
    return requests.get(URL + "/inventory")


def get_product_quantity(id):
    return requests.get(URL + "/inventory/" + str(id))


def purchase(id):
    return requests.put(URL + "/inventory/" + str(id))


print("Testing purchase with no coins")
r = purchase(1)
if r.status_code != 403 or r.headers["X-Coins"] != "0":
    print("Not enough coins test failed")
    print(r)

print("Testing add coin")
r = add_coin()
if r.status_code != 204 or r.headers["X-Coins"] != "1":
    print("Add coin test failed")
    print(r)

print("Testing reset purchase")
r = reset_purchase()
if r.status_code != 204 or r.headers["X-Coins"] != "1":
    print("Reset purchase test failed")
    print(r)

print("Testing purchase with 3 coins")
add_coin()
add_coin()
add_coin()
r = purchase(2)
if r.status_code != 200 or r.headers["X-Coins"] != "1" or r.headers["X-Inventory-Remaining"] != "4":
    print("First purchase on 2 failed")
    print(r.headers)

print("Testing get inventory")
r = get_inventory()
if r.status_code != 200 or re.sub(r"[\n\t\s]*", "", r.text) != "[5,5,4,5,5]":
    print("Get inventory failed")
    print(r)

print("Testing get product quantity")
r = get_product_quantity(2)
if r.status_code != 200 or r.text != "4":
    print("Get product quantity failed")
    print(r)

print("Buying all of one stock")
for n in range(1, 5):
    add_coin()
    add_coin()
    purchase(2)

print("Testing out of stock error")
add_coin()
add_coin()
add_coin()
r = purchase(2)
if r.status_code != 404 or r.headers["X-Coins"] != "3":
    print("Out of stock error failed")
    print(r.headers)


