import json

from flask import Flask, Response, request

import utilities
import vendomatic
from vendomatic import VendOMatic

app = Flask(__name__)
vending_machine = VendOMatic()


@app.route("/")
def root():
    return Response(
        json.dumps({"message": "Welcome to the Vend O Matic located in the Goodyear Tire you wish you weren't at on a "
                               "Sunday for 4 hours"}),
        status=200,
        content_type="application/json"
    )


@app.route("/", methods=["PUT"])
def accept_credits():
    # Since it is physically impossible to put anything but a single US quarter in we can assume this has succeeded
    vending_machine.insert_coin()
    res = Response(
        "",
        status=204,
        content_type="application/json"
    )

    res.headers["X-Coins"] = 1
    return res


@app.route("/", methods=["DELETE"])
def cancel_purchase():
    # Return all quarters
    quarters = vending_machine.cancel_purchase()
    res = Response(
        "",
        status=204,
        content_type="application/json"
    )

    res.headers["X-Coins"] = quarters
    return res


@app.route("/inventory", methods=["GET"])
def inventory():
    return Response(
        vending_machine.get_inventory(),
        status=200,
        content_type="application/json"
    )


@app.route("/inventory/<id>", methods=["GET"])
def get_product_quantity(id):
    if not utilities.is_valid_int(id, 0, 4):
        return Response(
            json.dumps({
                "error": "Invalid ID"
            }),
            status=400,
            content_type="application/json"
        )

    return Response(
        vending_machine.get_remaining_for_product(int(id)),
        status=200,
        content_type="application/json"
    )


@app.route("/inventory/<id>", methods=["PUT"])
def purchase_product(id):
    if not utilities.is_valid_int(id, 0, 4):
        return Response(
            json.dumps({
                "error": "Invalid ID"
            }),
            status=400,
            content_type="application/json"
        )

    try:
        coins, inventory_remaining = vending_machine.purchase_product(int(id))
        res = Response(
            json.dumps({
                "quantity": "1"
            }),
            status=200,
            content_type="application/json"
        )

        res.headers["X-Coins"] = coins
        res.headers["X-Inventory-Remaining"] = inventory_remaining

        return res

    except vendomatic.NotEnoughFundsException:
        res = Response(
            "",
            status=403
        )

        res.headers["X-Coins"] = vending_machine.get_credit()
        return res
    except vendomatic.OutOfStockException:
        res = Response(
            "",
            status=404,
        )

        res.headers["X-Coins"] = vending_machine.get_credit()
        return res

