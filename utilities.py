def is_valid_int(user_supplied_value: str, min: int, max: int) -> bool:
    try:
        int_value = int(user_supplied_value)
        if int_value > max or int_value < min:
            return False
    except:
        return False

    return True
