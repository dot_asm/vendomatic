import json


class VendOMatic():
    def __init__(self: "VendOMatic"):
        # How many coins have been supplied and not spent
        self.credit = 0
        self.price_per_item = 2
        # Simple array of ints to handle inventory ids: 0-4
        self.inventory = [5, 5, 5, 5, 5]

    def insert_coin(self: "VendOMatic"):
        self.credit += 1

    def cancel_purchase(self: "VendOMatic") -> int:
        returned_credit = self.credit
        self.credit = 0
        return returned_credit

    def get_inventory(self: "VendOMatic") -> str:
        return json.dumps(self.inventory)

    def get_remaining_for_product(self: "VendOMatic", id: int) -> str:
        return json.dumps(self.inventory[id])

    def get_credit(self: "VendOMatic") -> str:
        return json.dumps(self.credit)

    def purchase_product(self: "VendOMatic", id: int) -> (int, int):
        if self.credit < self.price_per_item:
            raise NotEnoughFundsException

        print(self.inventory[id])
        if self.inventory[id] == 0:
            raise OutOfStockException

        # Decrement inventory, return all quarters minus 2
        self.inventory[id] = self.inventory[id] - 1
        returned_credit = self.credit - 2
        self.credit = 0

        return returned_credit, self.inventory[id]


class OutOfStockException(Exception):
    """Raised when an item is out of stock"""
    pass


class NotEnoughFundsException(Exception):
    """Raised when there aren't enough credits for an item"""
    pass
